import csv, sys, os

project_dir = '/home/helen/Desktop/Projects/My_projects/lets_travel/lets_travel/'

sys.path.append(project_dir)

os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'

import django

django.setup()

from lets_travel_app.models import Country, City

data = csv.reader(open('/home/helen/Desktop/Projects/My_projects/lets_travel/simplemaps-worldcities-basic.csv'), delimiter=',')

country = None
for row in data:
    if row[0] != 'city':
        if country is None or country.name != row[1]:
            country = Country.objects.create(name=row[1])
        city = City.objects.create(city_name=row[0], country=country)
        country.save()
        city.save()