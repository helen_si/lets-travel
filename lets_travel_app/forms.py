from django.forms import ModelForm, Form
from django import forms
from .models import User, VisitedPlace, WishPlace, Comment


class CreatUserForm(ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ('username', 'email', 'password')


class LogInForm(Form):
    username = forms.CharField(max_length=30)
    password = forms.CharField(widget=forms.PasswordInput())


class EditUserProfileForm(ModelForm):

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'birthday', 'information','photo')


class VisitedPlaceForm(ModelForm):

    class Meta:
        model = VisitedPlace
        fields = ('country', 'cities', 'start_date', 'end_date','description')


class WishPlaceForm(ModelForm):

    class Meta:
        model = WishPlace
        fields = ('country', 'cities', 'description')


class CommentForm(ModelForm):

    class Meta:
        model = Comment
        fields = ('text',)
