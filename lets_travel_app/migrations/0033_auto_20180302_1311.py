# Generated by Django 2.0.1 on 2018-03-02 13:11

import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('lets_travel_app', '0032_auto_20180228_1930'),
    ]

    operations = [
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text', models.TextField(max_length=1000)),
                ('date', models.DateTimeField(default=datetime.datetime.now)),
                ('author', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='article',
            name='amount_of_comments',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='article',
            name='amount_of_likes',
            field=models.IntegerField(default=0),
        ),
    ]
