# Generated by Django 2.0.1 on 2018-03-08 17:03

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('lets_travel_app', '0045_wishplacecomment'),
    ]

    operations = [
        migrations.AlterField(
            model_name='wishplacecomment',
            name='wish_place',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='lets_travel_app.WishPlace'),
        ),
    ]
