# Generated by Django 2.0.1 on 2018-02-27 18:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lets_travel_app', '0025_auto_20180201_1600'),
    ]

    operations = [
        migrations.AddField(
            model_name='article',
            name='teaser',
            field=models.CharField(default='', max_length=100),
        ),
    ]
