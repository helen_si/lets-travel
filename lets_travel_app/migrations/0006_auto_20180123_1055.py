# Generated by Django 2.0.1 on 2018-01-23 10:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lets_travel_app', '0005_user_information'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='information',
            field=models.TextField(blank=True, default='', max_length=1000),
        ),
    ]
