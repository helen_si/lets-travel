# Generated by Django 2.0.1 on 2018-02-28 19:11

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lets_travel_app', '0030_countryinfo'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='countryinfo',
            options={'ordering': ('caption',), 'verbose_name_plural': 'Country Info'},
        ),
    ]
