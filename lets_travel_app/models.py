from django.db import models
from django.contrib.auth.models import PermissionsMixin, AbstractBaseUser, BaseUserManager


class UserManager(BaseUserManager):

    def _create_user(self, username, email, password, **extra_fields):
        if not email:
            raise ValueError('Please, add email')
        email = self.normalize_email(email)
        user = self.model(username=username, email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user


    def create_user(self, username, email, password, **extra_fields):
        extra_fields.setdefault('is_superuser', False)
        extra_fields.setdefault('is_staff', False)
        return self._create_user(username=username, email=email, password = password, **extra_fields)


    def create_superuser(self, username, email, password, **extra_fields):
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_staff', True)
        return self._create_user(username=username, email=email, password=password, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(max_length=30, unique=True)
    email = models.EmailField(unique=True)
    first_name = models.CharField(max_length=255, blank=True, default='')
    last_name = models.CharField(max_length=255, blank=True, default='')
    birthday = models.DateField(null=True, blank=True)
    photo = models.TextField(blank=True, default='')
    information = models.TextField(max_length=1000, blank=True, default='')
    is_staff = models.BooleanField(default=False)

    objects = UserManager()
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    class Meta:
        ordering = ('username',)


    def __str__(self):
        return "{}, {}".format(self.username, self.email)


    def get_full_name(self):
        full_name = []
        if self.first_name:
            full_name.append(self.first_name)
        if self.last_name:
            full_name.append(self.last_name)
        return " ".join(full_name)


class Country(models.Model):
    name = models.CharField(max_length=255)

    class Meta:
        verbose_name_plural = 'Countries'
        ordering = ('name',)

    def __str__(self):
        return f'{self.name}'


class City(models.Model):
    name = models.CharField(max_length=255)
    country = models.ForeignKey(Country, on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = 'Cities'
        ordering = ('name',)

    def __str__(self):
        return "{}: {}".format(self.country, self.name)


class VisitedPlace(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    country = models.ForeignKey(Country, on_delete=models.CASCADE)
    cities = models.ManyToManyField(City)
    start_date = models.DateField()
    end_date = models.DateField()
    description = models.TextField(max_length=1000, blank=True, default='')
    likes = models.ManyToManyField('Like', related_name='visited_places')
    comments = models.ManyToManyField('Comment', related_name='visited_places')


    class Meta:
        ordering = ('-start_date',)


    @property
    def all_cities(self):
        return ', '.join([x.name for x in self.cities.all()])


    def __str__(self):
        return "{}: {}".format(self.author.username, self.country)


class WishPlace(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    country = models.ForeignKey(Country, on_delete=models.CASCADE)
    cities = models.ManyToManyField(City)
    description = models.TextField(max_length=1000, blank=True, default='')
    created_date = models.DateTimeField(null=True)
    likes = models.ManyToManyField('Like', related_name='wish_places')
    comments = models.ManyToManyField('Comment', related_name='wish_places')

    class Meta:
        ordering = ('-created_date',)

    @property
    def all_cities(self):
        return ', '.join([city.name for city in self.cities.all()])


    def __str__(self):
        return "{}: {}".format(self.author.username, self.country)


class Article(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateTimeField()
    image = models.TextField(max_length=255)
    caption = models.CharField(max_length=50)
    teaser = models.CharField(max_length=100)
    article = models.TextField()
    likes = models.ManyToManyField('Like', related_name='articles')
    comments = models.ManyToManyField('Comment', related_name='articles')

    class Meta:
        ordering = ('-pk',)


    def __str__(self):
        return "id: {}. {}".format(self.id, self.caption)



class CountryInfo(models.Model):
    caption = models.CharField(max_length=40)
    teaser = models.CharField(max_length=100)
    info = models.TextField()
    image = models.TextField(max_length=255)


    class Meta:
        verbose_name_plural = 'Country Info'
        ordering = ('-pk',)


    def __str__(self):
        return "{}".format(self.caption)


class Comment(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    text = models.TextField(max_length=1000)
    date = models.DateTimeField()


    def __str__(self):
        return "{}: {}. {}".format(self.author, self.text, self.date)


class Like(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE)