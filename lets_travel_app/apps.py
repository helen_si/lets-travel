from django.apps import AppConfig


class LetsTravelAppConfig(AppConfig):
    name = 'lets_travel_app'
