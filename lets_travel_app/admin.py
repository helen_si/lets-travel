from django.contrib import admin
from .models import User, VisitedPlace, WishPlace, Article, Country, City, CountryInfo, Comment, Like
from django.contrib.auth.admin import UserAdmin


class CustomAdmin(UserAdmin):
    ordering = ('id',)
    list_display = ('id', 'username', 'email')
    list_filter = ()
    fieldsets = (
        ('Profile Information', {'fields': ('username', 'email', 'password')}),
    )


class VisitedPlaceAdmin(admin.ModelAdmin):
    ordering = ('id',)
    list_display = ('author', 'country')
    list_filter = ()
    fieldsets = (
        ('Profile Information', {'fields': ('author', 'country', 'cities', 'start_date', 'end_date', 'description')}),
    )


class WishPlaceAdmin(admin.ModelAdmin):
    ordering = ('id',)
    list_display = ('author', 'country', 'created_date')
    list_filter = ()
    fieldsets = (
        ('Profile Information', {'fields': ('author',  'created_date', 'country', 'description')}),
    )


class ArticleAdmin(admin.ModelAdmin):
    ordering = ('id',)
    list_display = ('caption', 'author', 'date')
    list_filter = ()
    fieldsets = (
        ('Profile Information', {'fields': ('caption', 'author', 'date', 'teaser', 'article', 'image')}),
    )


class CountryInfoAdmin(admin.ModelAdmin):
    ordering = ('id',)
    list_display = ('caption',)
    list_filter = ()
    fieldsets = (
        ('Profile Information', {'fields': ('caption', 'teaser', 'info', 'image')}),
    )


class CountryAdmin(admin.ModelAdmin):
    ordering = ('id',)
    list_display = ('name',)
    list_filter = ()
    fieldsets = (
        ('Profile Information', {'fields': ('name',)}),
    )


class CityAdmin(admin.ModelAdmin):
    ordering = ('id',)
    list_display = ('name', 'country')
    list_filter = ()
    fieldsets = (
        ('Profile Information', {'fields': ('name', 'country')}),
    )


class ArticleInline(admin.TabularInline):
    model = Article.likes.through



class LikeAdmin(admin.ModelAdmin):
    ordering = ('id',)
    list_display = ('author', )
    list_filter = ()
    filter_vertical = ('articles',)
    inlines = [ArticleInline]



class CommentAdmin(admin.ModelAdmin):
    ordering = ('-date',)
    list_display = ('author', 'date')
    list_filter = ()
    fieldsets = (
        ('Profile Information', {'fields': ('author', 'date', 'text')}),
    )


admin.site.register(User, CustomAdmin)
admin.site.register(VisitedPlace, VisitedPlaceAdmin)
admin.site.register(WishPlace, WishPlaceAdmin)
admin.site.register(Article, ArticleAdmin)
admin.site.register(CountryInfo, CountryInfoAdmin)
admin.site.register(Country, CountryAdmin)
admin.site.register(City, CityAdmin)
admin.site.register(Comment, CommentAdmin)
admin.site.register(Like, LikeAdmin)




