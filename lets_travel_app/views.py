from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from .models import User, VisitedPlace, WishPlace, Article, Country, City, CountryInfo, Comment, Like
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_GET, require_POST
from .forms import CreatUserForm, LogInForm, EditUserProfileForm, VisitedPlaceForm, WishPlaceForm, CommentForm
from django.contrib.auth import logout
from annoying.functions import get_object_or_None
from annoying.decorators import ajax_request
from datetime import datetime
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from django.template.loader import render_to_string


@require_GET
def index(request):
    users = User.objects.all()
    articles_on_homepage = Article.objects.order_by('-pk')[:4]
    countries_on_homepage = CountryInfo.objects.order_by('-pk')[:4]
    visited_places_on_homepage = VisitedPlace.objects.order_by('-pk')[:4]
    wish_places_on_homepage = WishPlace.objects.order_by('-pk')[:4]
    context = {
        'users': users,
        'articles_on_homepage': articles_on_homepage,
        'countries_on_homepage': countries_on_homepage,
        'visited_places_on_homepage': visited_places_on_homepage,
        'wish_places_on_homepage': wish_places_on_homepage
    }
    return render(request, 'lets_travel_app_templates/index.html', context=context)


@require_GET
def contact_us(request):
    return render(request, 'lets_travel_app_templates/contact_us.html')


@require_GET
def error_404(request):
    return render(request, '404.html')


@require_GET
def registration(request):
    return render(request, 'lets_travel_app_templates/registration.html')


@require_POST
def registration_process(request):
    form = CreatUserForm(request.POST)
    if form.is_valid():
        form.email = form.cleaned_data['email']
        form.username = form.cleaned_data['username']
        form.instance.set_password(form.cleaned_data['password'])
        user = form.save()
        if user:
            login(request, user)
            return redirect('lets_travel_app:index')
    else:
        return redirect('lets_travel_app:registration')


@require_GET
def login_form(request):
    return render(request, 'lets_travel_app_templates/login.html')


@require_POST
def login_process(request):
    form = LogInForm(request.POST)
    if form.is_valid():
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user = authenticate(request, username=username, password=password)
        if user:
            login(request, user)
            return redirect('lets_travel_app:index')
        else:
            return HttpResponse("Username or password is incorrect.")
    else:
        return redirect('lets_travel_app:login')


@require_GET
def logout_process(request):
    logout(request)
    return redirect('lets_travel_app:index')


@require_GET
def user_profile(request, username):
    user_profile = get_object_or_None(User, username=username)
    profile_owner = False
    authorized_guest = None
    if request.user.is_authenticated:
        if request.user.id == user_profile.id:
            profile_owner = True
        else:
            authorized_guest = request.user
    context = {
        'user_profile': user_profile,
        'profile_owner': profile_owner,
        'authorized_guest': authorized_guest,
    }
    return render(request, 'lets_travel_app_templates/user_profile.html', context=context)


@login_required
@require_GET
def edit_profile(request):
    user = request.user
    context = {
        'user': user
    }
    return render(request, 'lets_travel_app_templates/edit_profile.html', context=context)


@login_required
@require_POST
def edit_profile_process(request):
    form = EditUserProfileForm(request.POST, instance=request.user)
    if form.is_valid():
        form.save()
        username = request.user.username
        return redirect('lets_travel_app:user_profile', username=username)
    else:
        return redirect('lets_travel_app:edit_profile')


@login_required
@require_GET
def visited_places(request, username):
    user_profile = get_object_or_None(User, username=username)
    profile_owner = False
    visited_places = VisitedPlace.objects.filter(author__pk=user_profile.id)
    if request.user.is_authenticated:
        if request.user.id == user_profile.id:
            profile_owner = True
    context = {
        'visited_places': visited_places,
        'profile_owner': profile_owner,
        'user_profile': user_profile
    }
    return render(request, 'lets_travel_app_templates/visited_places.html', context=context)


@require_GET
def visited_place(request):
    visited_place_id = request.GET.get('id')
    visited_place = VisitedPlace.objects.prefetch_related('comments').get(pk=visited_place_id)
    is_liked = None
    profile_owner = False
    if request.user.is_authenticated:
        is_liked = get_object_or_None(Like, visited_places=visited_place, author=request.user) is not None
        if request.user.id == visited_place.author.id:
            profile_owner = True
    context = {
        'visited_place': visited_place,
        'profile_owner': profile_owner,
        'is_liked': is_liked
    }
    return render(request, 'lets_travel_app_templates/visited_place.html', context=context)


@login_required
@require_GET
def visited_place_form(request):
    countries = Country.objects.all()
    visited_place_form = None
    visited_place_form_id = request.GET.get('id')
    if visited_place_form_id:
        visited_place_form = get_object_or_None(VisitedPlace, pk=visited_place_form_id)
    context = {
        'visited_place_form': visited_place_form,
        'countries': countries
    }
    return render(request, 'lets_travel_app_templates/visited_place_form.html', context=context)


@login_required
@require_POST
def visited_place_process(request):
    username = request.user.username
    visited_place = None
    visited_place_id = request.POST.get('id')
    if visited_place_id:
        visited_place = get_object_or_None(VisitedPlace, pk=visited_place_id)
    form = VisitedPlaceForm(request.POST, instance=visited_place)
    if form.is_valid():
        visited_place = form.save(commit=False)
        visited_place.author = request.user
        visited_place.save()
        form.save_m2m()
        return redirect('lets_travel_app:visited_places', username=username)
    else:
        return redirect('lets_travel_app:visited_place_form')


@login_required
@require_POST
def delete_visited_place(request):
    username = request.user.username
    visited_place_id = request.POST.get('id')
    visited_place = get_object_or_None(VisitedPlace, pk=visited_place_id)
    visited_place.delete()
    return redirect('lets_travel_app:visited_places', username=username)


@require_GET
@login_required
def wish_list(request, username):
    user_profile = User.objects.get(username=username)
    profile_owner = False
    wish_list = WishPlace.objects.filter(author=user_profile)
    if request.user.is_authenticated:
        if request.user.id == user_profile.id:
            profile_owner = True
    context = {
        'wish_list': wish_list,
        'profile_owner': profile_owner,
    }
    return render(request, 'lets_travel_app_templates/wish_list.html', context=context)


@require_GET
def wish_place(request):
    wish_place_id = request.GET.get('id')
    wish_place = WishPlace.objects.prefetch_related('comments').get(pk=wish_place_id)
    is_liked = None
    profile_owner = False
    if request.user.is_authenticated:
        is_liked = get_object_or_None(Like, wish_places=wish_place, author=request.user) is not None
        if request.user.id == wish_place.author.id:
            profile_owner = True
    context = {
        'wish_place': wish_place,
        'profile_owner': profile_owner,
        'is_liked': is_liked
    }
    return render(request, 'lets_travel_app_templates/wish_place.html', context=context)


@login_required
@require_GET
def wish_place_form(request):
    countries = Country.objects.all()
    wish_place_form = None
    wish_place_form_id = request.GET.get('id')
    if wish_place_form_id:
        wish_place_form = get_object_or_None(WishPlace, pk=wish_place_form_id)
    context = {
        'wish_place_form': wish_place_form,
        'countries': countries
    }
    return render(request, 'lets_travel_app_templates/wish_place_form.html', context=context)


@login_required
@require_POST
def wish_place_process(request):
    username = request.user.username
    wish_place = None
    wish_place_id = request.POST.get('id')
    created_date = datetime.now()
    if wish_place_id:
        wish_place = get_object_or_None(WishPlace, pk=wish_place_id)
        if wish_place:
            created_date = wish_place.created_date
    form = WishPlaceForm(request.POST, instance=wish_place)
    if form.is_valid():
        wish_place = form.save(commit=False)
        wish_place.author = request.user
        wish_place.created_date = created_date
        wish_place.save()
        form.save_m2m()
        return redirect('lets_travel_app:wish_list', username=username)
    else:
        return redirect('lets_travel_app:wish_place_form')


@login_required
@require_POST
def delete_wish_place(request):
    username = request.user.username
    wish_place_id = request.POST.get('id')
    wish_place = get_object_or_None(WishPlace, pk=wish_place_id)
    wish_place.delete()
    return redirect('lets_travel_app:wish_list', username=username)


@require_GET
def articles(request):
    articles = Article.objects.all()
    context = {
        'articles': articles
    }
    return render(request, 'lets_travel_app_templates/articles.html', context=context)


@require_GET
def article(request):
    article_id = request.GET.get('id')
    article = Article.objects.prefetch_related('comments').get(pk=article_id)
    is_liked = None
    if request.user.is_authenticated:
        is_liked = get_object_or_None(Like, articles=article, author=request.user) is not None
    context = {
        'article': article,
        'is_liked': is_liked
    }
    return render(request, 'lets_travel_app_templates/article.html', context=context)


@require_GET
def countries(request):
    countries = CountryInfo.objects.all()
    context = {
        'countries': countries
    }
    return render(request, 'lets_travel_app_templates/countries.html', context=context)


@require_GET
def country(request):
    country_id = request.GET.get('id')
    country = get_object_or_None(CountryInfo, pk=country_id)
    context = {
        'country': country
    }
    return render(request, 'lets_travel_app_templates/country.html', context=context)


@require_GET
@login_required
@ajax_request
def cities_for_visited_place(request):
    country_id = request.GET.get('country_id')
    visited_place_id = request.GET.get('visited_place_id')
    visited_place = get_object_or_None(VisitedPlace, pk=visited_place_id)
    selected_cities = None
    if visited_place:
        selected_cities = visited_place.cities.all()
    if country_id:
        cities_list = []
        cities = City.objects.filter(country_id=country_id)
        for city in cities:
            is_selected = False
            if selected_cities:
                is_selected = True if city in selected_cities else False
            cities_list.append(
                {'name': city.name, 'id': city.id, 'is_selected': is_selected}
            )
        return {'success': True, 'cities': cities_list}
    else:
        return {'success': False}


@require_GET
@login_required
@ajax_request
def cities_for_wish_place(request):
    country_id = request.GET.get('country_id')
    wish_place_id = request.GET.get('wish_place_id')
    wish_place = get_object_or_None(WishPlace, pk=wish_place_id)
    selected_cities = None
    if wish_place:
        selected_cities = wish_place.cities.all()
    if country_id:
        cities_list = []
        cities = City.objects.filter(country_id=country_id)
        for city in cities:
            is_selected = False
            if selected_cities:
                is_selected = True if city in selected_cities else False
            cities_list.append(
                {'name': city.name, 'id': city.id, 'is_selected': is_selected}
            )
        return {'success': True, 'cities': cities_list}
    else:
        return {'success': False}


@csrf_exempt
@login_required
@require_POST
@ajax_request
def increment_likes_article(request):
    article_id = request.POST.get('article_id')
    article = get_object_or_None(Article, pk=article_id)
    like_change = 'Like'
    if not article:
        return {'success': False}
    like = get_object_or_None(Like, articles=article, author=request.user)
    if not like:
        article.likes.create(author=request.user)
        like_change = 'Liked'
    else:
        like.delete()
    article.save()
    amount_of_likes = article.likes.count()
    return {'success': True, 'amount_of_likes': amount_of_likes, 'like_change': like_change}


@csrf_exempt
@login_required
@require_POST
@ajax_request
def add_comment_article(request):
    article_id = request.POST.get('article_id')
    article = get_object_or_None(Article, pk=article_id)
    if not article:
        return {'success': False}
    form = CommentForm(request.POST)
    if article is not None and form.is_valid():
        comment = form.save(commit=False)
        comment.author = request.user
        comment.date = datetime.now()
        comment.save()
        article.comments.add(comment)
        amount_of_comments = article.comments.count()
        context = {
            'comment': comment,
        }
        html_str = render_to_string(
            'lets_travel_app_templates/snippets/comment.html', context, request)
        return {'success': True, 'html_str': html_str,'amount_of_comments': amount_of_comments}


@csrf_exempt
@login_required
@require_POST
@ajax_request
def delete_comment_article(request):
    article_id = request.POST.get('article_id')
    article = get_object_or_None(Article, pk=article_id)
    comment_id = request.POST.get('comment_id')
    comment = get_object_or_None(Comment, id=comment_id, author=request.user)
    if not comment and not article:
        return {'success': False}
    comment.delete()
    amount_of_comments = article.comments.count()
    return {'success': True,'amount_of_comments': amount_of_comments}


@csrf_exempt
@login_required
@require_POST
@ajax_request
def edit_comment_article(request):
    comment_id = request.POST.get('comment_id')
    comment = get_object_or_None(Comment, pk=comment_id, author=request.user)
    if not comment:
        return {'success': False}
    new_text = request.POST.get('edit_text')
    comment.text = new_text
    comment.save()
    return {'success': True,'new_text': new_text}


@csrf_exempt
@login_required
@require_POST
@ajax_request
def increment_likes_visited_place(request):
    visited_place_id = request.POST.get('visited_place_id')
    visited_place = get_object_or_None(VisitedPlace, pk=visited_place_id)
    like_change = 'Like'
    if not visited_place:
        return {'success': False}
    like = get_object_or_None(Like, visited_places=visited_place, author=request.user)
    if not like:
        visited_place.likes.create(author=request.user)
        like_change = 'Liked'
    else:
        like.delete()
    visited_place.save()
    amount_of_likes = visited_place.likes.count()
    return {'success': True, 'amount_of_likes': amount_of_likes, 'like_change': like_change}


@csrf_exempt
@login_required
@require_POST
@ajax_request
def add_comment_visited_place(request):
    visited_place_id = request.POST.get('visited_place_id')
    visited_place = get_object_or_None(VisitedPlace, pk=visited_place_id)
    if not visited_place:
        return {'success': False}
    form = CommentForm(request.POST)
    if visited_place is not None and form.is_valid():
        comment = form.save(commit=False)
        comment.author = request.user
        comment.date = datetime.now()
        comment.save()
        visited_place.comments.add(comment)
        amount_of_comments = visited_place.comments.count()
        context = {
            'comment': comment,
        }
        html_str = render_to_string(
            'lets_travel_app_templates/snippets/comment.html', context, request)
        return {'success': True, 'html_str': html_str,'amount_of_comments': amount_of_comments}


@csrf_exempt
@login_required
@require_POST
@ajax_request
def delete_comment_visited_place(request):
    visited_place_id = request.POST.get('visited_place_id')
    visited_place = get_object_or_None(VisitedPlace, pk=visited_place_id)
    comment_id = request.POST.get('comment_id')
    comment = get_object_or_None(Comment, id=comment_id, author=request.user)
    if not comment and not visited_place:
        return {'success': False}
    comment.delete()
    amount_of_comments = visited_place.comments.count()
    return {'success': True,'amount_of_comments': amount_of_comments}


@csrf_exempt
@login_required
@require_POST
@ajax_request
def edit_comment_visited_place(request):
    comment_id = request.POST.get('comment_id')
    comment = get_object_or_None(Comment, pk=comment_id, author=request.user)
    if not comment:
        return {'success': False}
    new_text = request.POST.get('edit_text')
    comment.text = new_text
    comment.save()
    return {'success': True,'new_text': new_text}


@csrf_exempt
@login_required
@require_POST
@ajax_request
def increment_likes_wish_place(request):
    wish_place_id = request.POST.get('wish_place_id')
    wish_place = get_object_or_None(WishPlace, pk=wish_place_id)
    like_change = 'Like'
    if not wish_place:
        return {'success': False}
    like = get_object_or_None(Like, wish_places=wish_place, author=request.user)
    if not like:
        wish_place.likes.create(author=request.user)
        like_change = 'Liked'
    else:
        like.delete()
    wish_place.save()
    amount_of_likes = wish_place.likes.count()
    return {'success': True, 'amount_of_likes': amount_of_likes, 'like_change': like_change}


@csrf_exempt
@login_required
@require_POST
@ajax_request
def add_comment_wish_place(request):
    wish_place_id = request.POST.get('wish_place_id')
    wish_place = get_object_or_None(WishPlace, pk=wish_place_id)
    if not wish_place:
        return {'success': False}
    form = CommentForm(request.POST)
    if wish_place is not None and form.is_valid():
        comment = form.save(commit=False)
        comment.author = request.user
        comment.date = datetime.now()
        comment.save()
        wish_place.comments.add(comment)
        amount_of_comments = wish_place.comments.count()
        context = {
            'comment': comment,
        }
        html_str = render_to_string(
            'lets_travel_app_templates/snippets/comment.html', context, request)
        return {'success': True, 'html_str': html_str,'amount_of_comments': amount_of_comments}


@csrf_exempt
@login_required
@require_POST
@ajax_request
def delete_comment_wish_place(request):
    wish_place_id = request.POST.get('wish_place_id')
    wish_place = get_object_or_None(WishPlace, pk=wish_place_id)
    comment_id = request.POST.get('comment_id')
    comment = get_object_or_None(Comment, id=comment_id, author=request.user)
    if not comment and not wish_place:
        return {'success': False}
    comment.delete()
    amount_of_comments = wish_place.comments.count()
    return {'success': True,'amount_of_comments': amount_of_comments}


@csrf_exempt
@login_required
@require_POST
@ajax_request
def edit_comment_wish_place(request):
    comment_id = request.POST.get('comment_id')
    comment = get_object_or_None(Comment, pk=comment_id, author=request.user)
    if not comment:
        return {'success': False}
    new_text = request.POST.get('edit_text')
    comment.text = new_text
    comment.save()
    return {'success': True,'new_text': new_text}